package com.example.macstudent.thenosegame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

public class Nose {

    int xPosition;
    int yPosition;
    int direction;
    Bitmap image;
    private Rect hitBox1, hitBox2;

    public Nose(Context context, int x, int y) {
        this.image = BitmapFactory.decodeResource(context.getResources(), R.drawable.nose01);
        this.xPosition = x;
        this.yPosition = y;

        this.hitBox1 = new Rect(
                this.xPosition + 100,
                this.yPosition + 350,
                this.xPosition + 281,
                this.yPosition + (this.image.getHeight() - 55)
        );

        this.hitBox2 = new Rect(
                this.xPosition + 330,
                this.yPosition + 350,
                this.xPosition + 510,
                this.yPosition + (this.image.getHeight() - 55)
        );

    }

    public Rect getHitBox1(){
        return this.hitBox1;
    }
    public Rect getHitBox2(){
        return this.hitBox2;
    }

    /*public void updateEnemyPosition() {
        this.xPosition = this.xPosition - 15;

        this.hitBox1.left = this.xPosition;
        this.hitBox1.right = (this.xPosition + this.image.getWidth());
        //this.hitBox.top = this.yPosition;
        //this.hitBox.bottom = this.yPosition + this.image.getHeight();
    }*/


    public void setXPosition(int x) {
        this.xPosition = x;
    }
    public void setYPosition(int y) {
        this.yPosition = y;
    }
    public int getXPosition() {
        return this.xPosition;
    }
    public int getYPosition() {
        return this.yPosition;
    }

    public Bitmap getBitmap() {
        return this.image;
    }

}
