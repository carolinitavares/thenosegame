package com.example.macstudent.thenosegame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.BoolRes;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {

    // Android debug variables
    final static String TAG="THE-NOSE";

    // screen size
    int screenHeight;
    int screenWidth;

    // game state
    boolean gameIsRunning;

    // threading
    Thread gameThread;


    // drawing variables
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    // ----------------------------
    // ## SPRITES
    // ----------------------------
    Finger player;
    Nose enemy;

    // ----------------------------
    // ## GAME STATS
    // ----------------------------
    int scoreWin = 0;
    int scoreLose = 0;
    //int lives = 5;
    //Bitmap backgroudImages;

    //SHOW BOUNDARIES AND HITBOXES
    Boolean showHitBoxes = false;
    Boolean showRect = false;


    int MARGIN = 0;
    boolean hitTheBox = false;
    Rect rect;
    int xPosition;
    int yPosition;

    private Paint paint;


    public GameEngine(Context context, int w, int h) {
        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;


        this.MARGIN = (int) (w * 0.15);

        this.xPosition = w;
        this.yPosition = h;
        this.rect = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + (xPosition - MARGIN),
                this.yPosition + (xPosition - MARGIN)
        );

        //if (showRect){
            //canvas.drawRect(this.rect, paintbrush);
            // create the Paint and set its color
            //paint = new Paint();
            //paint.setColor(Color.GRAY);
        //}
        this.printScreenInfo();

        // @TODO: Add your sprites
        this.spawnPlayer();
        this.spawnEnemyShips();
        // @TODO: Any other game setup
        //backgroudImages = BitmapFactory.decodeResource(context.getResources(), R.drawable.background);
        //backgroudImages = Bitmap.createScaledBitmap(backgroudImages, this.screenWidth, this.screenHeight, false);

    }

    public Rect getRectBox(){
        return this.rect;
    }

   /* @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.BLUE);
        canvas.drawRect(rect, paint);
    }*/

    private void printScreenInfo() {

        Log.d(TAG, "Screen (w, h) = " + this.screenWidth + "," + this.screenHeight);
    }

    private void spawnPlayer() {
        // put player in middle of screen --> you may have to adjust the Y position
        // depending on your device / emulator


        Log.d(TAG, "Margin = " + MARGIN);

        player = new Finger(this.getContext(), MARGIN, this.screenHeight - 450 );
        player.setScreenWidth(this.screenWidth);
        player.setScreenHeight(this.screenHeight);


    }
    private void spawnEnemyShips() {
        Random random = new Random();

        //@TODO: Place the enemies in a random location
        enemy = new Nose(this.getContext(), (this.screenWidth-550)/2, (this.screenHeight-1100));
    }

    // ------------------------------
    // GAME STATE FUNCTIONS (run, stop, start)
    // ------------------------------
    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }


    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            // Error
        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }


    // ------------------------------
    // GAME ENGINE FUNCTIONS
    // - update, draw, setFPS
    // ------------------------------

    public void updatePositions() {
        // @TODO: Update position of player
        player.setMargin(MARGIN);
        player.setScreenWidth(this.screenWidth);
        player.setScreenHeight(this.screenHeight);
        player.updatePlayerPosition();

        // @TODO: Collision detection between player and enemy
        if (player.getHitBox().intersect(enemy.getHitBox1())) { //CAROL
            // reduce lives
            scoreWin++;
            player.setDirection(-1);
            this.hitTheBox = true;

            try {
                gameThread.sleep(1000);/*Thread.sleep(1000);*/
            }
            catch (InterruptedException ex) {
                android.util.Log.d("THE NOSE", ex.toString());
            }
        }

        else if (player.getHitBox().intersect(enemy.getHitBox2())) { //CAROL
            // reduce lives
            scoreWin++;
            player.setDirection(-1);
            this.hitTheBox = true;

            try {
                gameThread.sleep(1000);/*Thread.sleep(1000);*/
            }
            catch (InterruptedException ex) {
                android.util.Log.d("THE NOSE", ex.toString());
            }
        }

        else{
            if (player.getDirection() == 1 && player.getYPosition() == 0){
                scoreLose++;

                try {
                    gameThread.sleep(1000);/*Thread.sleep(1000);*/
                }
                catch (InterruptedException ex) {
                    android.util.Log.d("THE NOSE", ex.toString());
                }
            }
        }
    }

    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            //----------------

            // configure the drawing tools
            this.canvas.drawColor(Color.argb(255,255,255,255));
            paintbrush.setColor(Color.WHITE);

            //@TODO: draw background
            //canvas.drawBitmap(this.backgroudImages, 0, 0, paintbrush);

            //@TODO: Draw the player
            canvas.drawBitmap(this.player.getBitmap(), this.player.getXPosition(), this.player.getYPosition(), paintbrush);

            paintbrush.setColor(Color.RED);
            paintbrush.setStrokeWidth(5);
            paintbrush.setStyle(Paint.Style.STROKE);

            if (showHitBoxes){
                canvas.drawRect(player.getHitBox(), paintbrush);
            }

            //@TODO: Draw the enemy
            canvas.drawBitmap(this.enemy.getBitmap(), this.enemy.getXPosition(), this.enemy.getYPosition(), paintbrush);

            if (showHitBoxes) {
                canvas.drawRect(enemy.getHitBox1(), paintbrush);
                canvas.drawRect(enemy.getHitBox2(), paintbrush);
            }

            if (showRect){
                //canvas.drawRect(this.getRectBox(), paintbrush);
                canvas.drawRect(player.getScreenBox(), paintbrush);
            }

            //@TODO:  Draw text on screen
            paintbrush.setTextSize(50);
            canvas.drawText("Nose picked " + this.scoreWin + " times", 30, 100, paintbrush);
            canvas.drawText("Nose missed " + this.scoreLose + " times", 30, 200, paintbrush);

            if (player.getDirection() == 1 && player.getYPosition() == 0) {
                paintbrush.setColor(Color.BLACK);
                paintbrush.setTextSize(60);
                this.canvas.drawText("YOU LOSE!!!", this.screenWidth / 2, this.screenHeight / 2, paintbrush);

                reestart();

            }else if(this.hitTheBox){
                paintbrush.setColor(Color.BLACK);
                paintbrush.setTextSize(60);
                this.canvas.drawText("WIN!!!", this.screenWidth / 2, this.screenHeight / 2, paintbrush);

                reestart();
            }

            //----------------
            this.holder.unlockCanvasAndPost(canvas);

        }
    }

    public void reestart(){
        this.hitTheBox = false;
        player.setDirection(0);
        player.setXPosition(MARGIN);
        player.setYPosition(this.screenHeight - 450);
    }

    public void setFPS() {
        try {
            gameThread.sleep(50);
        }
        catch (Exception e) {

        }
    }

    // ------------------------------
    // USER INPUT FUNCTIONS
    // ------------------------------

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {
            // move player up
            this.player.setMargin(MARGIN);
            this.player.setDirection(1);
        }
        else if (userAction == MotionEvent.ACTION_UP) {
            // move player down
            //this.player.setDirection(0);
        }

        return true;
    }
}
