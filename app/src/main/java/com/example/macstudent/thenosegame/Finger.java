package com.example.macstudent.thenosegame;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;

public class Finger {

    int xPosition;
    int yPosition;
    int direction = 0;              // -1 = not moving, 0 = down, 1 = up
    Bitmap playerImage;
    private Rect hitBox;
    Rect screenBox;
    int margin;
    int screenWidth;
    int screenHeight;
    final int SPEEDLR = 25; //LEFT RIGHT
    final int SPEEDUP = 15; //UP
    boolean isGoingRight = true;

    public Finger(Context context, int x, int y) {
        this.playerImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.finger01);
        this.xPosition = x;
        this.yPosition = y;

        Log.d("CAROL", "W = " + screenWidth);

        this.hitBox = new Rect(
                this.xPosition,
                this.yPosition,
                this.xPosition + this.playerImage.getWidth(),
                this.yPosition + this.playerImage.getHeight()
        );

        this.screenBox = new Rect(
                this.xPosition,
                this.yPosition + 260,
                this.screenWidth + 1610,
                this.screenHeight
        );

    }

    public Rect getHitBox(){
        return this.hitBox;
    }

    public  Rect getScreenBox(){
        return this.screenBox;
    }

    public void updatePlayerPosition() {
        if (this.direction == 0) {
            // sides
            if(isGoingRight){
                if (this.xPosition <= (screenWidth - margin)/*margin*/) {
                    this.xPosition = this.xPosition + SPEEDLR;
                }
                else{
                    isGoingRight = false;
                }
            }

            if(isGoingRight == false){
                if (this.xPosition >= margin/*(screenWidth - margin)*/) {
                    this.xPosition = this.xPosition - SPEEDLR;
                }
                else{
                    isGoingRight = true;
                }
            }
        }
        else if (this.direction == 1) {
            // move up
           if (this.yPosition == 0){
                Log.d("CAROL", "LOOSER");
            }
            else{
                this.yPosition = this.yPosition - SPEEDUP;
            }

        }
        else if (this.direction == -1){
            //do not move
            //this.xPosition = this.xPosition;
            //this.yPosition = this.yPosition;
        }


        this.hitBox.left = this.xPosition;
        this.hitBox.right = (this.xPosition + this.playerImage.getWidth());
        this.hitBox.top = this.yPosition;
        this.hitBox.bottom = this.yPosition + this.playerImage.getHeight();
    }

    public void setXPosition(int x) {
        this.xPosition = x;
    }
    public void setYPosition(int y) {
        this.yPosition = y;
    }
    public void setMargin(int margin){
        this.margin = margin;
    }
    public void setScreenWidth(int width) {
        this.screenWidth = width;
    }
    public void setScreenHeight(int height){
        this.screenHeight = height;
    }
    public int getXPosition() {
        return this.xPosition;
    }
    public int getYPosition() {
        return this.yPosition;
    }
    public int getDirection(){
        return  this.direction;
    }

    /**
     * Sets the direction of the player
     * @param i     0 = SIDES, 1 = up
     */
    public void setDirection(int i) {
        this.direction = i;
        //this.margin = margin;
    }
    public Bitmap getBitmap() {
        return this.playerImage;
    }


}
